import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsNotEmpty, IsArray, IsUrl, IsBooleanString, MinDate, MaxDate, IsString} from "class-validator";

class Pelicula {

    @Length(2, 25)
    titulo: string;

    @IsDate()
    @MinDate(new Date('1950-01-01'))
    @MaxDate(new Date())
    fechaLanzamiento: Date;

    @IsArray()
    @IsNotEmpty()
    actoresPrincipales: string[];

    @Min(0)
    @Max(10)
    rating: number;

    @IsNotEmpty({
        message: "Sinopsis es obligatoria"
    })
    @IsString()
    sinopsis: string;

    @IsBooleanString()
    nominadaAOscar: boolean;

    @IsUrl()
    pagina: string;

}

let pelicula = new Pelicula();
pelicula.titulo = 'A';
pelicula.fechaLanzamiento = new Date('1800-01-01');
pelicula.actoresPrincipales = [];
pelicula.rating = -0.1;
pelicula.sinopsis = '';
pelicula.nominadaAOscar = true;
pelicula.pagina = 'google@com';

validate(pelicula, { validationError: { target: false } })
.then(erroresValidacion => {
    const hayErroresValidacion = erroresValidacion.length > 0;
    if(hayErroresValidacion){
        console.log("validation failed. errors: ", erroresValidacion);
    }else{
        console.log("validation succeed");
    }
})




// {
//     "compilerOptions": {
//         "experimentalDecorators": true,
//         "allowJs": true
//     }
// }