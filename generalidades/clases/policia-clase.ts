import {UsuarioFacil} from './usuario-clase'

export class Policia extends UsuarioFacil {
    constructor(
        nombre, cedula, public rango:string, fechaUnionAPolicia?
    ){
        super(nombre, cedula, fechaUnionAPolicia);
        rango = rango
    }
}