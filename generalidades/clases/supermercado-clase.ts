import {UsuarioFacil} from './usuario-clase'

export class SuperMercado extends UsuarioFacil {
    constructor(
        nombreOrganizacion, RUC, fechaFundacion, public sucursales: number
    ){
        super(nombreOrganizacion, RUC, fechaFundacion);
        sucursales = sucursales;
    }
}