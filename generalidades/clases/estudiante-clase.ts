import {UsuarioFacil} from './usuario-clase'

export class EstudiantePoli extends UsuarioFacil {
    constructor(
        nombre, cedula, public carrera, public semestre, fechaInicio?
    ){
        super(nombre, cedula, fechaInicio);
        semestre = semestre;
        carrera = carrera;
    }
}