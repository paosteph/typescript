export class Usuario {

    constructor(
        protected nombre: string, 
        protected cedulaRuc:string, 
        protected fecha?: string)
        {
            this.nombre = nombre;
            this.cedulaRuc = cedulaRuc;
            this.fecha = fecha;
        }
    
    get Nombre(){
        return this.nombre;
    }
    set Nombre(nombre){
        this.nombre = nombre;
    }
    get CedulaRuc(){
        return this.cedulaRuc;
    }
    set CedulaRuc(cedulaRuc){
        this.cedulaRuc = cedulaRuc;
    }
    get Fecha(){
        return this.fecha;
    }
    set Fecha(fecha){
        this.fecha = fecha;
    }
}