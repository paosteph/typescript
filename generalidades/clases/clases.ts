import {Policia} from './policia-clase';
import {SuperMercado} from './supermercado-clase';
import {EstudiantePoli} from './estudiante-clase';

export {
    Policia,
    SuperMercado,
    EstudiantePoli
}