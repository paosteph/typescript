interface usuarioInterface {
    nombre: string;
    apellido: string;
}

interface correoInterface {
    correo: string;
    enviarCorreo(): boolean;
}

const usuario: usuarioInterface = {
    nombre: 'pao',
    apellido: 'guamani'
}

interface conTodo extends correoInterface, usuarioInterface{
    completo: boolean;
}

usuario.nombre = 'steph';
usuario.apellido = 'campos';

class usuarioClase implements conTodo {
    completo: boolean;    
    correo: string;
    enviarCorreo(): boolean {
        throw new Error("Method not implemented.");
    }
    nombre: string;
    apellido: string;

    
}