import {Policia, SuperMercado, EstudiantePoli} from './clases/clases';

const persona: object = {
    nombre: 'pao',
    apellido: 'guamani'
};

const numero:number = 55;
const arreglo: Array<number | string | boolean> = [2,true];
const arregloDos:(string | number)[] = [2,'a'];

function sumar(valorUno?: number, valorDos?: number): number{
    return valorUno + valorDos;
}

sumar(2,2);

// class Persona {
//     private _nombre: string;
//     private _apellido: string;
//     public edad: number;

//     constructor(protected nombre:string, protected apellido?:string){
//         this._nombre = nombre;
//         this._apellido = apellido;
//         this.edad = 0;
//     }

// }

// class Estudiante extends Persona{
//     public carrera: string;
//     constructor(public nombre: string, public apellido: string, carrera: string){
//         super(nombre, apellido);
//         this.carrera = carrera;
//     }
// }

// const personaDos: Persona = new Persona('pao');
// const estudiante:Estudiante = new Estudiante('paola', 'guamani','sistemas')



console.log(new EstudiantePoli('pao', 'guamani', 'sis', '7'));
console.log(new SuperMercado('SuperMaxi', '4141', '55',8));


