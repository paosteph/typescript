enum Semaforo {
    verde = 'VERDE',
    amarillo = 'AMARILLO',
    rojo = 'ROJO'
}

enum Respuesta {
    si = 1,
    no = 0
}